import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import ReleaseNotesPage  from './pages/ReleaseNotesPage';
import MainPage  from './pages/MainPage';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueRouter)

const routes = [
  { path: '/releasenotes', name: 'releasenotes', component: ReleaseNotesPage, meta: {title: 'Release Notes'} },
  { path: '/', name: 'main', component: MainPage, meta: {title: 'Omega Tab Manager'} },
  { path: '/*', redirect: '/' }, // redirect all to main page
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || 'Omega Tab Manager';
  next();
});


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
